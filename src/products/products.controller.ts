import { Controller, UseFilters, UseGuards, Get, Query, Param } from '@nestjs/common';

import { ProductsService } from './products.service';
import { ProductQueryDto } from '../models/products/product-query.dto';
import { AuthGuard } from '@nestjs/passport';
import { Product } from '../data/entities/product.entity';
import { FoodGroup } from '../data/entities/food-group.entity';
import { ProductsDto } from '../models/products/products.dto';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) { }

  // @Get()
  // // @UseGuards(AuthGuard())
  // async getProducts(@Query() query: ProductQueryDto) {
  //   const route: string = `localhost:3000/products`;

  //   return this.productsService.getProducts(query, route);
  // }

  @Get('query')
  async queryProducts(@Query('page') page: number = 0, @Query('limit') limit: number = 20,
                      @Query('food') food: number = 0, @Query('string') searchString: string = ''): Promise<ProductsDto> {
    limit = limit > 20 ? 20 : limit;
    return await this.productsService.getQueryProducts({ page, limit, foodcode: food, description: searchString });
  }

  @Get()
  async index(@Query('page') page: number = 0, @Query('limit') limit: number = 20,
    // @Query('food') food: number = 0, @Query('string') searchString: string = ''
    ) {
    limit = limit > 100 ? 100 : limit;
    const queryOptions: ProductQueryDto = {};
    // if (food) {
    //   queryOptions.foodGroupFoodcode = food;
    // }
    // if (searchString.length) {
    //   queryOptions.description = `Like("%${searchString}%")`;
    // }
    return await this.productsService.getProducts({ page, limit, route: 'http://localhost:3000/products' }, queryOptions);
  }

  @Get('count')
  async count(): Promise<number> {
    return this.productsService.getAllProductsNumber();
  }
  @Get('groups')
  async getAllProductGroups(): Promise<FoodGroup[]> {
    return await this.productsService.getAllProductGroups();
  }

  @Get(':id')
  async getASingleProduct(@Param('id') productId: number): Promise<Product> {
    return this.productsService.getASingleProduct(productId);
  }

}
