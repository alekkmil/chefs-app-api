import { FoodGroup } from './../data/entities/food-group.entity';
import { ProductsDto } from './../models/products/products.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { paginate, Pagination, IPaginationOptions } from 'nestjs-typeorm-paginate';
import { Product } from './../data/entities/product.entity';
import { ProductQueryDto } from './../models/products/product-query.dto';
import { ProductRO } from '../models/products/product-ro';
import { IMeasure } from '../common/interfaces/measure';
import { INutrition } from '../common/interfaces/nutrition';
import { async } from 'rxjs/internal/scheduler/async';
import { string } from 'joi';

@Injectable()
export class ProductsService {
  constructor(@InjectRepository(Product) private readonly productRepository: Repository<Product>,
              @InjectRepository(FoodGroup) private readonly foodGroupRepository: Repository<FoodGroup>) { }

  async getProducts(options: IPaginationOptions, queryOptions: ProductQueryDto): Promise<Pagination<Product>> {
    return await paginate<Product>(this.productRepository, options, queryOptions);

    // const description = query.description ? query.description : '';
    // const foodGroup = query.foodGroup ? query.foodGroup : '';
    // let limit =  query.limit ? +query.limit : 0;
    // limit = limit > 100 ? 100 : limit;
    // let page = query.page ? +query.page : 1;
    // page = page < 0 ? 1 : page;
    // let queryStr = `${route}?`;

    // const queryBuilder = await this.productRepository
    //   .createQueryBuilder('product')
    //   .leftJoinAndSelect('product.measures', 'measure')
    //   .leftJoinAndSelect('product.nutrition', 'nutrition')
    //   .leftJoin('product.recipeProducts', 'recipeProducts')
    //   .addOrderBy('product.description', 'ASC');

    // if (description) {
    //   queryBuilder.where('LOWER(product.description) LIKE :description', {
    //     description: `%${description.toLowerCase()}%`,
    //   });
    //   queryStr = queryStr.concat(`description=${description}&`);
    // }

    // if (foodGroup) {
    //   queryBuilder.andWhere('LOWER(product.foodGroup) LIKE :foodGroup', {
    //     foodGroup: `%${foodGroup.toLowerCase()}%`,
    //   });
    //   queryStr = queryStr.concat(`foodGroup=${foodGroup}&`);
    // }

    // if (limit) {
    //   queryBuilder.take(limit).skip((page - 1) * limit);
    //   queryStr = queryStr.concat(`limit=${limit}&`);
    // }

    // const products =  await queryBuilder.getMany();
    // const productsROArr = products.map((prod) => this.productToRO(prod));

    // const total =  await queryBuilder.getCount();
    // const isNext = limit ? route && (total / limit >= page) : false;
    // const isPrevious = route && page > 1;
    // const productsToReturn = new ProductsDto();
    // productsToReturn.products = productsROArr;
    // productsToReturn.page = page;
    // productsToReturn.productsCount = total < limit || limit === 0 ? total : limit;
    // productsToReturn.totalProducts = total;
    // productsToReturn.next = isNext ? `${queryStr}page=${page + 1}` : '';
    // productsToReturn.previous = isPrevious ? `${queryStr}page=${page - 1}` : '';

    // return productsToReturn;
  }

  // private productToRO(product: Product): ProductRO {
  //   const measures: IMeasure[] = product.measures.map((msr) => {
  //     const measureToReturn: IMeasure = {
  //       measure: msr.measure,
  //       gramsPerMeasure: msr.gramsPerMeasure,
  //     };

  //     return measureToReturn;
  //   });
  //   const nutrition: INutrition = {
  //       PROCNT: product.nutrition.PROCNT,
  //       FAT: product.nutrition.FAT,
  //       CHOCDF: product.nutrition.CHOCDF,
  //       ENERC_KCAL: product.nutrition.ENERC_KCAL,
  //       SUGAR: product.nutrition.SUGAR,
  //       FIBTG: product.nutrition.FIBTG,
  //       CA: product.nutrition.CA,
  //       FE: product.nutrition.FE,
  //       P: product.nutrition.P,
  //       K: product.nutrition.K,
  //       NA: product.nutrition.NA,
  //       VITA_IU: product.nutrition.VITA_IU,
  //       TOCPHA: product.nutrition.TOCPHA,
  //       VITD: product.nutrition.VITD,
  //       VITC: product.nutrition.VITC,
  //       VITB12: product.nutrition.VITB12,
  //       FOLAC: product.nutrition.FOLAC,
  //       CHOLE: product.nutrition.CHOLE,
  //       FATRN: product.nutrition.FATRN,
  //       FASAT: product.nutrition.FASAT,
  //       FAMS: product.nutrition.FAMS,
  //       FAPU: product.nutrition.FAPU,
  //     };

  //   const productRO = {
  //     description: product.description,
  //     // foodGroup: product.foodGroup,
  //     measures,
  //     nutrition,
  //   };

  //   return productRO;
  // }

  async getQueryProducts(query: ProductQueryDto): Promise<ProductsDto> {
    const productsPerPage = query.limit;
    let foundProducts: Product[];
    let count: number;
    const search = query.description;
    const page = query.page;
    if ((!search || search === '') && query.foodcode && +query.foodcode !== 0) {
      foundProducts = await this.productRepository.find({
        where: [{
          foodGroup: query.foodcode,
        }],
        take: productsPerPage,
        skip: productsPerPage * (page - 1),
      });
      // console.log('group')
      // console.log(foundProducts)
      count = await this.productRepository.count({
        where: [{
          foodGroup: query.foodcode,
        }],
      });

    } else if (search.length > 0 && query.foodcode && +query.foodcode !== 0) {
      // console.log('group+regexp')
      // console.log(foundProducts)
      foundProducts = await this.productRepository.find({
        where: {
          description: Like(`%${search}%`),
          foodGroup: query.foodcode,
        },
        take: productsPerPage,
        skip: productsPerPage * (page - 1),
      });

      count = await this.productRepository.count({
        where: {
          description: Like(`%${search}%`),
          foodGroup: query.foodcode,
        },
      });
    } else if ((!search || search === '') && query.foodcode && +query.foodcode === 0 ) {
      foundProducts = await this.productRepository.find({
        take: productsPerPage,
        skip: productsPerPage * (page - 1),
      });
      count = await this.productRepository.count();
    } else {
      foundProducts = await this.productRepository.find({
        where: {
          description: Like(`%${search}%`),
        },
        take: productsPerPage,
        skip: productsPerPage * (page - 1),
      });
      // console.log('regexp')
      // console.log(foundProducts)
      count = await this.productRepository.count({
        where: {
          description: Like(`%${search}%`),
        },
      });
    }

    const returnDTO: ProductsDto = {
      products: foundProducts,

      productsCount: productsPerPage,
      totalProducts: count,
    };

    return returnDTO;
  }

  async getASingleProduct(productId: number): Promise<Product> {
    const productsToReturn: Product = await this.productRepository.findOne({
      where: {
        code: productId,
      },
    });
    const measures = productsToReturn.measures;
    const measuresToPrint = measures.map(m => m.measure);
    return productsToReturn;
  }
  async getAllProductsNumber(): Promise<number> {
    const countProducts: number = await this.productRepository.count();
    return countProducts;
  }

  async getAllProductGroups(): Promise<FoodGroup[]> {
    const foodGroups: FoodGroup[] = await this.foodGroupRepository.find();
    return foodGroups;
  }
}
