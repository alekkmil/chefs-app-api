import { INutrient } from '../../common/interfaces/nutrient';

export class ShowIngredientDTO {
    productDescription: string;
    productCode: number;
    amount: number;
    nutrients: INutrient[];
}
