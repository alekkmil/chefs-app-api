import { IsString, IsNumber } from 'class-validator';
import { ArgumentOutOfRangeError } from 'rxjs';

export class IngridientDTO {
    @IsNumber()
    productCode: number;

    @IsNumber()
    amount: number;
}
