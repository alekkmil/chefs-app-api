import { Product } from '../../data/entities/product.entity';

export class NutrientConverter {
    amount: number;
    product: Product;
}
