import { ShowIngredientDTO } from './showIngredient.dto';
import { RecipeNutritionInfo } from '../../common/interfaces/recipeNutrition';
import { ReturnRecipeDTO } from './returnRecipe.dto';

export class ReturnComplexRecipe {
  id: string;
  author: string;
  title: string;
  type: string;
  date: Date;
  subRecipes: ReturnRecipeDTO[];
  ingredients: ShowIngredientDTO[];
  recipeNutrition: RecipeNutritionInfo;
}
