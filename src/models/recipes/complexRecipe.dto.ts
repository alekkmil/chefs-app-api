import { IsString, IsDefined } from 'class-validator';
import { ShowIngredientDTO } from './showIngredient.dto';
import { IngridientDTO } from './ingredient.dto';
import { Recipe } from '../../data/entities/recipe.entity';
import { CreateRecipeDTO } from './createRecipe.dto';

export class CreateComplexRecipeDTO {
    @IsString()
    title: string;

    @IsDefined()
    subRecipes: string[];

    @IsDefined()
    ingridients: IngridientDTO[];
}
