import { IsString, MinLength, Matches, IsDefined } from 'class-validator';
import { RecipeType } from '../../common/enums/recipeType';
import { Ingredient } from '../../data/entities/ingridient.entity';
import { IngridientDTO } from './ingredient.dto';

export class CreateRecipeDTO {
 @IsString()
 title: string;

 @IsString()
 recipeType: string;

 @IsDefined()
 ingridients: IngridientDTO[];

}
