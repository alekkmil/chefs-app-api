import { IsString } from 'class-validator';
import { Ingredient } from '../../data/entities/ingridient.entity';
import { INutrient } from '../../common/interfaces/nutrient';
import { IngridientDTO } from './ingredient.dto';
import { ShowIngredientDTO } from './showIngredient.dto';
import { RecipeNutritionInfo } from '../../common/interfaces/recipeNutrition';

export class ReturnRecipeDTO {
  id: string;
  author: string;
  title: string;
  type: string;
  date: Date;
  parentRecipes: string[];
  ingredients: ShowIngredientDTO[];
  recipeNutrition: RecipeNutritionInfo;
}
