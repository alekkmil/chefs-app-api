import { INutrition } from '../../common/interfaces/nutrition';
import { IMeasure } from '../../common/interfaces/measure';

export interface ProductRO {
  description: string;
  // foodGroup: foodGroup;
  measures: IMeasure[];
  nutrition: INutrition;
}
