export class ProductQueryDto {
  description?: string;
  foodcode?: number;
  page?: number;
  limit?: number;
}
