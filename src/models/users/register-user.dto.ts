import { IsEmail, IsString, Matches, IsNotEmpty, MinLength } from 'class-validator';

export class RegisterUserDTO {
  @IsString()
  @MinLength(3)
  username: string;

  @IsString()
  // @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;
}
