import { Entity, Column, OneToMany, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Product } from './product.entity';

@Entity('measures')
export class Measure {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Product, product => product.measures)
  product: Promise<Product>;

  @Column()
  measure: string;

  @Column()
  gramsPerMeasure: number;
}
