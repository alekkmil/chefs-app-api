import { Product } from './product.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Recipe } from './recipe.entity';
import { INutrient } from '../../common/interfaces/nutrient';
import { ComplexRecipe } from './complexRecipe.entity';

@Entity('ingredients')
export class Ingredient {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Product, product => product.ingredients , { eager: true })
  product: Product;

  @Column({ default: 0 })
  amount: number;

  @ManyToOne(type => Recipe, recipe => recipe.ingredients , {nullable: true} )
  recipe: Promise<Recipe>;

  @ManyToOne(type => ComplexRecipe, complexRecipe => complexRecipe.ingredients, {nullable: true})
  complexRecipe: Promise<ComplexRecipe>;
}
