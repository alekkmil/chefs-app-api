import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, CreateDateColumn, UpdateDateColumn, ManyToMany } from 'typeorm';

import { Ingredient } from './ingridient.entity';
import { User } from './user.entity';
import { Nutrition } from './nutrition.entity';
import { MinLength } from 'class-validator';
import { FoodGroup } from './food-group.entity';
import { RecipeType } from '../../common/enums/recipeType';
import { ComplexRecipe } from './complexRecipe.entity';
import { RecipeNutritionInfo } from '../../common/interfaces/recipeNutrition';

@Entity('recipes')
export class Recipe {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  @MinLength(3)
  title: string;

  @ManyToOne(type => User, user => user.recipes)
  author: Promise<User>;

  @Column()
  recipeType: RecipeType;

  @OneToMany(type => Ingredient, ingredient => ingredient.recipe , { eager: true })
  ingredients: Ingredient[];

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @Column({default: false})
  isDeleted: boolean;

  @ManyToMany(type => ComplexRecipe, complxRecipe => complxRecipe.recipes)
  complexRecipe: Promise<ComplexRecipe[]>;

}
