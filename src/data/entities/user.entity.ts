import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';

import { Recipe } from './recipe.entity';

@Entity('users')
export class User {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  username: string;
  @Column()
  password: string;

  @CreateDateColumn()
  joined: Date;

  @OneToMany(type => Recipe, recipe => recipe.author)
  recipes: Promise<Recipe[]>;
}
