import { Entity, PrimaryGeneratedColumn, OneToMany, ManyToOne, Column, CreateDateColumn, JoinTable, ManyToMany } from 'typeorm';
import { Recipe } from './recipe.entity';
import { Ingredient } from './ingridient.entity';
import { User } from './user.entity';
import { MinLength } from 'class-validator';

@Entity('complex-recipes')
export class ComplexRecipe {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  @MinLength(3)
  title: string;

  @ManyToMany(type => Recipe, recipe => recipe.complexRecipe, {eager: true})
  @JoinTable()
  recipes: Recipe[];

  @OneToMany(type => Ingredient, ingredient => ingredient.complexRecipe, {eager: true})
  ingredients: Ingredient[];

  @Column( {default: false})
  isDeleted: boolean;

  @CreateDateColumn()
  createdOn: Date;

  @ManyToOne(type => User, user => user.recipes)
  author: Promise<User>;
}
