import { Entity, Column, OneToMany, PrimaryGeneratedColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { Product } from './product.entity';
import { Recipe } from './recipe.entity';

@Entity('foodGroup')
export class FoodGroup {
  @PrimaryColumn()
  foodcode: number;
  @Column()
  description: string;
  @OneToMany(type => Product, product => product.foodGroup)
  product: Promise<Product[]>;
}
