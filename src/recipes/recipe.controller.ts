import { Controller, Body, Post, ValidationPipe, UseGuards, Req, Get, Delete, Param, BadRequestException, Put } from '@nestjs/common';
import { CreateRecipeDTO } from '../models/recipes/createRecipe.dto';
import { async } from 'rxjs/internal/scheduler/async';
import { RecipeService } from './recipe.service';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../data/entities/user.entity';
import { ReturnRecipeDTO } from '../models/recipes/returnRecipe.dto';
import { CreateComplexRecipeDTO } from '../models/recipes/complexRecipe.dto';
import { ReturnComplexRecipe } from '../models/recipes/returnComplexRecipe';

@Controller('recipes')
export class RecipeController {

 constructor(
     private readonly recipeService: RecipeService,
 ) {}

 @UseGuards(AuthGuard('jwt'))
 @Post()
 async createRecipe(@Body(new ValidationPipe({ transform: true, whitelist: true }))
                    recipeToCreate: CreateRecipeDTO ,
                    @Req() request: any): Promise<ReturnRecipeDTO> {
    const foundRecipe = await this.recipeService.findRecipeByName(recipeToCreate.title, request.user);
    if (foundRecipe) {
       throw new BadRequestException('A recipe with that name already exists');
    }
    return this.recipeService.createRecipe(recipeToCreate , request.user);
 }

 @UseGuards(AuthGuard('jwt'))
 @Get()
 async getAllRecepies(@Req() request: any): Promise<ReturnRecipeDTO[]> {
    return this.recipeService.getAllRecipes(request.user);
 }

 @UseGuards(AuthGuard('jwt'))
 @Get(':id')
 async getSingleRecipe(@Req() request: any, @Param('id') recipeId: string): Promise<ReturnRecipeDTO> {
    const foundRecipe = await this.recipeService.findRecipeById(recipeId);
    if (!foundRecipe) {
       throw new BadRequestException('There is no such recipe');
    }
    return this.recipeService.getSingleRecipe(recipeId, request.user.username);
 }

 @UseGuards(AuthGuard('jwt'))
 @Delete(':id')
 async deleteARecipe(@Req() request: any, @Param('id') id: string): Promise<{message: string}> {
   const foundRecipe = await this.recipeService.findRecipeById(id);
   if (!foundRecipe) {
      throw new BadRequestException('There is no such recipe');
   }
   return this.recipeService.deleteRecipe(id);
 }

 @UseGuards(AuthGuard('jwt'))
 @Put(':id')
 async updateRecipe(@Body(new ValidationPipe({ transform: true, whitelist: true })) recipeToUpd: CreateRecipeDTO,
                    @Req() request: any, @Param('id') id: string): Promise<{message: 'success'}> {
   const foundRecipe = await this.recipeService.findRecipeById(id);
   if (!foundRecipe) {
      throw new BadRequestException('There is no such recipe');
   }
   return await this.recipeService.updateRecipe(id, recipeToUpd, request.user);
 }

}
