import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Recipe } from '../data/entities/recipe.entity';
import { CreateRecipeDTO } from '../models/recipes/createRecipe.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { Ingredient } from '../data/entities/ingridient.entity';
import { RecipeType } from '../common/enums/recipeType';
import { ReturnRecipeDTO } from '../models/recipes/returnRecipe.dto';
import { RecipesHelperService } from './recipes-helper/recipes-helper.service';
import { CreateComplexRecipeDTO } from '../models/recipes/complexRecipe.dto';
import { ComplexRecipe } from '../data/entities/complexRecipe.entity';
import { ReturnComplexRecipe } from '../models/recipes/returnComplexRecipe';

@Injectable()
export class RecipeService {

constructor(
    @InjectRepository(Recipe) private readonly recipeRepository: Repository<Recipe>,
    private readonly recipeHelperService: RecipesHelperService,
    @InjectRepository(ComplexRecipe) private readonly mealsRepository: Repository<ComplexRecipe>,
    @InjectRepository(Ingredient) private readonly ingredinetRepo: Repository<Ingredient>,
) {}

    async createRecipe(recipeToCreate: CreateRecipeDTO , user: User): Promise<ReturnRecipeDTO> {
        const recipeToAdd = new Recipe();

        recipeToAdd.recipeType = RecipeType[recipeToCreate.recipeType];
        recipeToAdd.title = recipeToCreate.title;
        recipeToAdd.author = Promise.resolve(user);
        const ingredientArray: Ingredient[] = await this.recipeHelperService.createIngredients(recipeToCreate.ingridients);
        Promise.all(ingredientArray).then( value => recipeToAdd.ingredients = value );
        await this.recipeRepository.save(recipeToAdd);
        return this.recipeHelperService.convertRecipe(recipeToAdd, user.username);
    }

    async  findRecipeById(id: string): Promise<Recipe> {
        return this.recipeRepository.findOne({where: {id, isDeleted: false}});
    }

    async  findRecipeByName(name: string, user: User): Promise<Recipe> {
        return this.recipeRepository.findOne({where: {title: name, isDeleted: false, author: user}});
    }

    async  findMealById(id: string): Promise<ComplexRecipe> {
        return this.mealsRepository.findOne({where: {id, isDeleted: false}});
    }

    async getAllRecipes(author: User): Promise<ReturnRecipeDTO[]> {
        const userRecipes = await this.recipeRepository.find({
            where: {
                author,
                isDeleted: false,
            },
        });
        const recipesToReturn = Promise.all(
        userRecipes.map( async recipe =>
         await this.recipeHelperService.convertRecipe(recipe, author.username)));

        return recipesToReturn;
    }

    async deleteRecipe(id: string): Promise<{message: string}> {
        await this.recipeRepository.update({id} , {isDeleted: true});
        return {message: 'success'};
    }

    async createComplexRecipe(complexRecipe: CreateComplexRecipeDTO, user: User) {
        const complexRecipeToAdd = new ComplexRecipe();
        complexRecipeToAdd.author = Promise.resolve(user);

        const foundRecipes = await this.recipeRepository.findByIds(complexRecipe.subRecipes);

        complexRecipeToAdd.recipes = foundRecipes;
        complexRecipeToAdd.title = complexRecipe.title;
        const ingredientArray: Ingredient[] = await this.recipeHelperService.createIngredients(complexRecipe.ingridients);
        complexRecipeToAdd.ingredients = ingredientArray;
        await this.mealsRepository.save(complexRecipeToAdd);
        foundRecipes.map( async recipe => {
            const mealsArray = await recipe.complexRecipe;

            mealsArray.push(complexRecipeToAdd);
            recipe.complexRecipe = Promise.resolve(mealsArray);
            await this.recipeRepository.save(recipe);

        });
        return await this.recipeHelperService.calculateComplexRecipeNutrition(complexRecipeToAdd);
    }

    async getSingleRecipe(recipeId: string, username: string) {
        const foundRecipe = await this.recipeRepository.findOne(recipeId);

        return await this.recipeHelperService.convertRecipe(foundRecipe , username);
    }

    async getAllComplexRecipes(author: User): Promise<ReturnComplexRecipe[]> {
        const userRecipes = await this.mealsRepository.find({
            where: {
                author,
                isDeleted: false,
            },
        });
        const recipesToReturn: ReturnComplexRecipe[] = await Promise.all(userRecipes.map(recipe =>
         (this.recipeHelperService.calculateComplexRecipeNutrition(recipe))));

        return recipesToReturn;
    }

    async getASingleComplexRecipe(user: User, recipeId: string): Promise<ReturnComplexRecipe> {
        const foundRecipe = await this.mealsRepository.findOne(recipeId);
        return this.recipeHelperService.calculateComplexRecipeNutrition(foundRecipe);
    }

    async deleteMeal(id: string): Promise<{message: string}> {
        this.mealsRepository.update({id}, {isDeleted: true});
        return {message: 'success'};
    }

    async updateRecipe(id: string, recipeToUpd: CreateRecipeDTO, user: User): Promise<{message: 'success'}> {
        const ingredientArray = await this.recipeHelperService.createIngredients(recipeToUpd.ingridients);
        const foundRecipe = await this.recipeRepository.findOne({id});
        foundRecipe.ingredients = ingredientArray;
        foundRecipe.title = recipeToUpd.title;

        await this.recipeRepository.save(foundRecipe);
        return {message: 'success'};
    }
   async updateMeal(id: string, recipeToUpd: CreateComplexRecipeDTO): Promise<{message: string}> {
       const foundRеcipe = await this.mealsRepository.findOne({id});
       const ingredientArray = await this.recipeHelperService.createIngredients(recipeToUpd.ingridients);
       const foundRecipes = await this.recipeRepository.findByIds(recipeToUpd.subRecipes);
       foundRеcipe.title = recipeToUpd.title;
       foundRеcipe.ingredients = ingredientArray;
       foundRеcipe.recipes = foundRecipes;
       await this.mealsRepository.save(foundRеcipe);
       foundRecipes.map( async recipe => {
        const mealsArray = await recipe.complexRecipe;
        mealsArray.push(foundRеcipe);
        recipe.complexRecipe = Promise.resolve(mealsArray);
        await this.recipeRepository.save(recipe);
    });
       return {message: 'success'};
   }
}
