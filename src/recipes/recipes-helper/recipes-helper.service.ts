import { Injectable } from '@nestjs/common';
import { Recipe } from '../../data/entities/recipe.entity';
import { ReturnRecipeDTO } from '../../models/recipes/returnRecipe.dto';
import { Ingredient } from '../../data/entities/ingridient.entity';
import { ShowIngredientDTO } from '../../models/recipes/showIngredient.dto';
import { IngridientDTO } from '../../models/recipes/ingredient.dto';
import { Product } from '../../data/entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RecipeType } from '../../common/enums/recipeType';
import { RecipeNutritionInfo } from '../../common/interfaces/recipeNutrition';
import { ComplexRecipe } from '../../data/entities/complexRecipe.entity';
import { async } from 'rxjs/internal/scheduler/async';
import { ReturnComplexRecipe } from '../../models/recipes/returnComplexRecipe';

@Injectable()
export class RecipesHelperService {
    constructor(
        @InjectRepository(Ingredient) private readonly ingredientRepository: Repository<Ingredient>,
        @InjectRepository(Product) private readonly productRepository: Repository<Product>,
    ) { }

    async convertRecipe(recipe: Recipe, author: string): Promise<ReturnRecipeDTO> {
        const ingredients: Ingredient[] = recipe.ingredients;

        const ingredientsToAdd = this.transformIngredients(ingredients);
        const complexRecipes = await recipe.complexRecipe;
        const recipeNames = complexRecipes.map( r => {
            if (r.isDeleted === false) {
           return r.title;
         }});

        const recipeNutrition = await this.calculateRecipeNutrition(ingredientsToAdd);
        const recipeToReturn: ReturnRecipeDTO = {
            parentRecipes: recipeNames,
            id: recipe.id,
            date: recipe.createdOn,
            title: recipe.title,
            type: RecipeType[recipe.recipeType],
            author,
            ingredients: ingredientsToAdd,
            recipeNutrition,
        };

        return recipeToReturn;

    }
    transformIngredients(ingredients: Ingredient[]): ShowIngredientDTO[] {
        const transformedIngr = ingredients.map( (ing) => {
            const ingredientToadd: ShowIngredientDTO = {
                amount: ing.amount,
                productDescription: ing.product.description,
                productCode: ing.product.code,
                nutrients: [{
                    description: ing.product.nutrition.CA.description,
                    unit: ing.product.nutrition.CA.unit,
                    value: (ing.product.nutrition.CA.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.FAT.description,
                    unit: ing.product.nutrition.FAT.unit,
                    value: (ing.product.nutrition.FAT.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.PROCNT.description,
                    unit: ing.product.nutrition.PROCNT.unit,
                    value: (ing.product.nutrition.PROCNT.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.CHOCDF.description,
                    unit: ing.product.nutrition.CHOCDF.unit,
                    value: (ing.product.nutrition.CHOCDF.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.ENERC_KCAL.description,
                    unit: ing.product.nutrition.ENERC_KCAL.unit,
                    value: (ing.product.nutrition.ENERC_KCAL.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.SUGAR.description,
                    unit: ing.product.nutrition.SUGAR.unit,
                    value: (ing.product.nutrition.SUGAR.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.NA.description,
                    unit: ing.product.nutrition.NA.unit,
                    value: (ing.product.nutrition.NA.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.FIBTG.description,
                    unit: ing.product.nutrition.FIBTG.unit,
                    value: (ing.product.nutrition.FIBTG.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.FE.description,
                    unit: ing.product.nutrition.FE.unit,
                    value: (ing.product.nutrition.FE.value * ing.amount) / 100,
                }, {
                    description: ing.product.nutrition.VITD.description,
                    unit: ing.product.nutrition.VITD.unit,
                    value: (ing.product.nutrition.VITD.value * ing.amount) / 100,
                },
                ],
            };

            return ingredientToadd;
        });
        return transformedIngr;
    }
    async createIngredients(ingredientDTOArray: IngridientDTO[]): Promise<Ingredient[]> {
        const ArrToReturn: Ingredient[] = [];
        const productCodes = ingredientDTOArray.map(ingr => ingr.productCode);
        const products: Product[] = await this.productRepository.findByIds(productCodes);
        for (const ingredient of ingredientDTOArray) {
            const ingredientToAdd: Ingredient = new Ingredient();
            ingredientToAdd.recipe = null;
            ingredientToAdd.complexRecipe = null;
            ingredientToAdd.amount = ingredient.amount;
            ingredientToAdd.product = products.filter((prod) => prod.code === +ingredient.productCode)[0];
            ArrToReturn.push(ingredientToAdd);
            await this.ingredientRepository.save(ingredientToAdd);
        }
        return ArrToReturn;
    }

    async calculateRecipeNutrition(ingredients: ShowIngredientDTO[]): Promise<RecipeNutritionInfo> {
        const nutrients = ingredients.map(ing => ing.nutrients);
        const recipeNutrition = {
            CHOCDF: {
                description: 'Carbohydrates',
                unit: 'g',
                value: 0,
            },
            FAT: {
                description: 'Total fat',
                unit: 'g',
                value: 0,
            },
            PROCNT: {
                description: 'Protein',
                unit: 'g',
                value: 0,
            },
            ENERC_KCAL: {
                description: 'Energy',
                unit: 'kcal',
                value: 0,
            },
            NA: {
                description: 'Sodium',
                unit: 'mg',
                value: 0,
            },
            SUGAR: {
                description: 'Sugars, total',
                unit: 'g',
                value: 0,
            },
            FIBTG: {
                description: 'Fiber, total dietary',
                unit: 'g',
                value: 0,
            },
            FE: {
                description: 'Iron, Fe',
                unit: 'g',
                value: 0,
            },
            P: {
                description: 'Calcium, Ca',
                unit: 'mg',
                value: 0,
            },
            VITD: {
                description: 'Vitamin D',
                unit: 'mg',
                value: 0,
            },
        };
        nutrients.map(nuts => nuts.map(nut => {
            switch (nut.description) {
                case 'Protein':
                    recipeNutrition.PROCNT.value += nut.value;
                    break;
                case 'Total lipid (fat)':
                    recipeNutrition.FAT.value += nut.value;
                    break;
                case 'Carbohydrate, by difference':
                    recipeNutrition.CHOCDF.value += nut.value;
                    break;
                case 'Energy':
                    recipeNutrition.ENERC_KCAL.value += nut.value;
                    break;
                case 'Sodium, Na':
                    recipeNutrition.NA.value += nut.value;
                    break;
                case 'Sugars, total':
                    recipeNutrition.SUGAR.value += nut.value;
                    break;
                case 'Fiber, total dietary':
                    recipeNutrition.FIBTG.value += nut.value;
                    break;
                case 'Iron, Fe':
                    recipeNutrition.FE.value += nut.value;
                    break;
                case 'Calcium, Ca':
                    recipeNutrition.P.value += nut.value;
                    break;
                case 'Vitamin D':
                    recipeNutrition.VITD.value += nut.value;
                    break;
            }
        }));
        return recipeNutrition;
    }

    async calculateComplexRecipeNutrition(complexRecipe: ComplexRecipe) {
        const complexRecipeToRO = new ReturnComplexRecipe();
        const user = await complexRecipe.author;

        complexRecipeToRO.author = user.username;
        complexRecipeToRO.title = complexRecipe.title;
        complexRecipeToRO.type = 'meal';
        complexRecipeToRO.date = complexRecipe.createdOn;
        complexRecipeToRO.id = complexRecipe.id;
        const convRecipesArray: ReturnRecipeDTO[] = [];
        for (const recipe of complexRecipe.recipes) {
            if (recipe.isDeleted === false) {
                const recipeToPush = await this.convertRecipe(recipe , user.username);
                convRecipesArray.push(recipeToPush);
            }
        }
        complexRecipeToRO.subRecipes = convRecipesArray;
        const transformedIngr = this.transformIngredients(complexRecipe.ingredients);
        complexRecipeToRO.ingredients = transformedIngr;
        const subRecipeIngredients = [];
        complexRecipeToRO.subRecipes.map( recipe => recipe.ingredients
                                    .map(ingr => subRecipeIngredients.push(ingr)));
        const ingredinetsToCalculate = subRecipeIngredients.concat(complexRecipeToRO.ingredients);
        const recipeNutritionValue = await this.calculateRecipeNutrition(ingredinetsToCalculate);
        complexRecipeToRO.recipeNutrition = recipeNutritionValue;

        return complexRecipeToRO;
    }
}
