import { Test, TestingModule } from '@nestjs/testing';
import { RecipesHelperService } from './recipes-helper.service';

describe('RecipesHelperService', () => {
  let service: RecipesHelperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecipesHelperService],
    }).compile();

    service = module.get<RecipesHelperService>(RecipesHelperService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
