import { Controller, UseGuards, Post, Body, ValidationPipe, Req, Get, Param, BadRequestException, Delete, Put} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateComplexRecipeDTO } from '../../models/recipes/complexRecipe.dto';
import { ReturnComplexRecipe } from '../../models/recipes/returnComplexRecipe';
import { RecipeService } from '../recipe.service';

@Controller('meals')
export class MealsController {
    constructor(
        private readonly recipeService: RecipeService,
        ) {}
    @UseGuards(AuthGuard('jwt'))
    @Post()
    async createComplexRecipe(@Body(new ValidationPipe({ transform: true, whitelist: true }))
                              recipeToCreate: CreateComplexRecipeDTO ,
                              @Req() request: any): Promise<ReturnComplexRecipe> {
       return await this.recipeService.createComplexRecipe(recipeToCreate , request.user);
    }

     @UseGuards(AuthGuard('jwt'))
     @Get()
     async getAllComplexRecipes(@Req() request: any): Promise<ReturnComplexRecipe[]> {
     return await this.recipeService.getAllComplexRecipes(request.user);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get(':id')
    async getASingleComplexRecipe(@Req() request: any, @Param('id') recipeId): Promise<ReturnComplexRecipe> {
    const foundRecipe = await this.recipeService.findMealById(recipeId);
    if (!foundRecipe) {
        throw new BadRequestException('Meal not found');
    }
    return await this.recipeService.getASingleComplexRecipe(request.user, recipeId);
   }

   @UseGuards(AuthGuard('jwt'))
   @Delete(':id')
   async deleteMeal(@Req() request: any, @Param('id') recipeId): Promise<{message: string}> {
   const foundRecipe = await this.recipeService.findMealById(recipeId);
   if (!foundRecipe) {
       throw new BadRequestException('Meal not found');
   }
   return await this.recipeService.deleteMeal(recipeId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  async updateRecipe(@Body(new ValidationPipe({ transform: true, whitelist: true })) recipeToUpd: CreateComplexRecipeDTO,
                     @Req() request: any, @Param('id') id: string): Promise<{message: string}> {
    const foundRecipe = await this.recipeService.findMealById(id);
    if (!foundRecipe) {
       throw new BadRequestException('There is no such recipe');
    }
    return await this.recipeService.updateMeal(id, recipeToUpd);
  }
}
