import { Module } from '@nestjs/common';
import { RecipeController } from './recipe.controller';
import { RecipeService } from './recipe.service';
import { Recipe } from '../data/entities/recipe.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from '../core/core.module';
import { Product } from '../data/entities/product.entity';
import { Ingredient } from '../data/entities/ingridient.entity';
import { RecipesHelperService } from './recipes-helper/recipes-helper.service';
import { ComplexRecipe } from '../data/entities/complexRecipe.entity';
import { MealsController } from './meals/meals.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Recipe, Product, Ingredient, ComplexRecipe, Ingredient]), CoreModule],
  controllers: [RecipeController, MealsController],
  providers: [RecipeService, RecipesHelperService],
})
export class RecipeModule {}
