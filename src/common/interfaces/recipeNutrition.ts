import { INutrient } from './nutrient';

export interface RecipeNutritionInfo {
    PROCNT: INutrient;
    FAT: INutrient;
    CHOCDF: INutrient;
    ENERC_KCAL: INutrient;
    NA: INutrient;
    FE: INutrient;
    SUGAR: INutrient;
    FIBTG: INutrient;
    P: INutrient;
    VITD: INutrient;
}
