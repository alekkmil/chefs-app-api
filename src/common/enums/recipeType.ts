export enum RecipeType {
  sauce,
  protein,
  salad,
  garnish,
  dessert,
}
